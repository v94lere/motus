import unittest

class TestMotusGame(unittest.TestCase):
    def test_check_word(self):
        word = "PYTHON"
        guess = "POTION"
        result = check_word(word, guess)
        expected = [f"\033[91mP\033[0m", "O", f"\033[93mT\033[0m", "I", "O", f"\033[91mN\033[0m"]
        self.assertEqual(result, expected)
    
    def test_invalid_word(self):
        wordlist = ["PYTHON"]
        with self.assertRaises(SystemExit): 
            play_game(wordlist)

if __name__ == "__main__":
    unittest.main()
