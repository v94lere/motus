import random

# Liste statique des mots de 6 lettres
wordlist = [
    "PYTHON", "FUSION", "ORDURE", "PRINCE", "DEMONS", "GARDER", "GLACON",
    "GOUTTE", "JARDIN", "LIVRES", "MANGER", "NIVEAU", "PARLER", "QUOTAS",
    "ROUTES", "SALOON", "TARDER", "URGENS", "WEBLOG", "ZEBRES"
]

def display_grid(grid):
    for row in grid:
        print(" ".join(row))
    print("\n")

def check_word(word, guess):
    result = []
    for i, char in enumerate(guess):
        if char == word[i]:
            result.append(f"\033[91m{char}\033[0m")  # Red for correct position
        elif char in word:
            result.append(f"\033[93m{char}\033[0m")  # Yellow for wrong position
        else:
            result.append(char)
    return result

def play_game(wordlist):
    word = random.choice(wordlist)
    grid = [['_' for _ in range(6)] for _ in range(7)]
    grid[0][0] = word[0]
    
    attempts = 0
    while attempts < 7:
        display_grid(grid)
        guess = input(f"Tentative {attempts + 1}/7 : ").strip().upper()
        result = check_word(word, guess)
        grid[attempts] = result
        if guess == word:
            print("Félicitations ! Vous avez trouvé le mot.")
            display_grid(grid)
            return
        attempts += 1
    print(f"Vous avez perdu ! Le mot était {word}")

if __name__ == "__main__":
    play_game(wordlist)
