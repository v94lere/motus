# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import random

class motus(models.Model):
    _name = 'motus.game'

    name = fields.Char(string='Name', required=True, size=6)
    input = fields.Char(string='Input', readonly=True)
    solution = fields.Char(string='Solution', default=lambda self: self._default_solution(), insivible=True)
    error = fields.Integer(string='Error', readonly=True)
    history = fields.Text(string='History', default='', readonly=True, insivible=True)
    status = fields.Selection([
        ('win', 'Win'),
        ('playing', 'Playing')
    ], string='Status', readonly=True, insivible=True)

    @api.model
    def _default_solution(self):
        self.error = 0
        self.history = ''
        return random.choice(self.wordlist)
    
    wordlist = [
    "PYTHON", "MOTUS", "FUSION", "ORDURE", "PRINCE", "DEMONS", "GARDER", "GLACON",
    "GOUTTE", "JARDIN", "LIVRES", "MANGER", "NIVEAU", "OBEISS", "PARLER", "QUOTAS",
    "ROUTES", "SALOON", "TARDER", "URGENS", "VICTOI", "WEBLOG", "ZEBRES"
]
    @api.onchange('name')
    def _onchange_name(self):
        if not isinstance(self.name, str) or not isinstance(self.solution, str):
            return
        self.name = self.name.upper()
        self.input = '' 
        self.error += 1
        for x in range (0, 6):
            if self.name[x] == self.solution[x]:
                self.input += self.name[x]
            else:
                self.input += '_'
        if self.name == self.solution:
            self.status = 'win'
            raise UserError(_("WIN !")).unlink()
        else:
            if self.error >= 7:
                raise UserError(_("You lose ! The solution was %s")).unlink()