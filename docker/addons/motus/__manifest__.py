{
    'name': 'Motus',
    'version': '1.0',
    'summary': 'Create motus in Odoo 17',
    'author': 'Valère NEVEUX',
    'depends': ['base'],
    'data': [
        'views/motus_views.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}